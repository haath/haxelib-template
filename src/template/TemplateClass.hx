package template;

/**
 * Documentation of the template class.
 */
class TemplateClass
{
    public function new()
    {
    }

    /**
     * Documentation of the error method.
     */
    public function errorMethod()
    {
        throw 'some error.';
    }

    /**
     * Foo method to test doc generation
     *
     * @param x input variable
     * @return output description
     */
    function foo(x: Int): Bool
    {
        return true;
    }
}
