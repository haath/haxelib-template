#!/bin/bash

REF=${CI_COMMIT_TAG:-master}

haxelib run dox -o public -i build/types.xml \
    -in ^template.*$ \
    -D source-path https://gitlab.com/haath/haxelib-template/-/tree/${REF}/src/ \
    -D website https://gitlab.com/haath/haxelib-template \
    -D logo https://gitlab.com/uploads/-/system/project/avatar/19075408/haxe-logo-glyph.png?width=64 \
    --title "Template API documentation" \
    -D description  "Template API documentation"

