<div align="center">

### haxelib-template

[![build status](https://gitlab.com/haath/udprotean/badges/master/pipeline.svg)](https://gitlab.com/haath/udprotean/pipelines)
[![test coverage](https://gitlab.com/haath/udprotean/badges/master/coverage.svg)](https://gitlab.com/haath/udprotean/-/jobs/artifacts/master/browse?job=test-neko)
[![license](https://img.shields.io/badge/license-MIT-blue.svg?style=flat)](https://gitlab.com/haath/udprotean/blob/master/LICENSEs)
[![haxelib version](https://badgen.net/haxelib/v/udprotean)](https://lib.haxe.org/p/udprotean)
[![haxelib downloads](https://badgen.net/haxelib/d/udprotean)](https://lib.haxe.org/p/udprotean)

</div>

---

Template for Haxelib library projects, including:

- Unit test structure with [munit](https://lib.haxe.org/p/munit) and [mcover](https://lib.haxe.org/p/mcover/).
- Gitlab CI pipeline for running tests on multiple platforms, and reporting the test coverage.
- Pipeline step for releasing to [lib.haxe.org](https://lib.haxe.org) automatically from every `v*.*.*` tag.
- Pipeline step for generating API documenation and publishing to Gitlab Pages.


## How to use

1. Fork or clone this repository.
2. Update `haxelib.json` with the library name and description.
3. Update `LICENSE` with the correct name.
4. Add `HAXELIB_PASSWORD` as a CI environment variable, with the correct password from `haxelib register`.
5. Put library classes under `src/`.
6. Put tests in separate classes under `test/tests/`. (see: `TemplateTest.hx`)
7. Replace `udprotean` in the README badges with the project name.
8. Edit `hxml/gen_doc.sh` to replace `template` with the package name.
9. If there are dependencies, add the `-lib` entries to `hxml/doc.hxml` as well.


### Releasing versions

To publish a new version of the library, create a git tag with this exact format: `v1.2.3`

This will cause the CI to push a new version `1.2.3` of the library.

